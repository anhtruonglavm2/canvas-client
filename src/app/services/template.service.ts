import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  templateUrl = environment.api + "template/"
  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) { }

  createTemplate(data) {
    return this.http.post(this.templateUrl+"addTemplate", data).subscribe((res) => {
      this.toastr.success("Added Succesfully");
    });
  }

  getTemplates(category) {
    return this.http.get(this.templateUrl+"getTemplates",{params: {'category': category}})
  }

  getAllTemplates() {
    return this.http.get(this.templateUrl+"getAllTemplates")
  }

  deleteCategory(category) {
    return this.http.delete(this.templateUrl+"deleteCategory",{params: {'category': category}}).subscribe(res => {
      this.toastr.success("Deleted Succesfully")
      this.router.navigate(['/admin/dashboard'])
    });
  }

  deleteTemplate(key){
    this.http.delete(this.templateUrl+"deleteTemplate", {params: {key:key}}).subscribe(res => {
      this.toastr.success("Deleted Succesfully")
      setTimeout(()=>{
        window.location.reload();
      },1000)
    });
  }
}
