import { Component, OnInit } from '@angular/core';
import { TemplateService } from '../../services/template.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {

  displayedColumns: string[] = [ 
    'image', 
    'lastModified',
    'options'
  ];

  awsurl = "https://apito-canvas.s3.ap-south-1.amazonaws.com/";
  
  canvasCat = [
    {
      viewValue: 'Birthday',
      value: 'Birthday'
    },
    {
      viewValue: 'Anniversary',
      value: 'Anniversary'
    },
    {
      viewValue: 'Other',
      value: 'Other'
    }
  ];

  pickedCategory = "Anniversary";

  templates:any[];

  dialogRef: any;

  constructor(private templateService: TemplateService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getTemplates();
  }

  getTemplates() {
   this.templateService.getTemplates(this.pickedCategory).subscribe((res: any) => {
      this.templates = res.Contents;
   });
  }

  fetchCategory() {
    this.getTemplates();
  }

  openModal(templateRef) {
    this.dialogRef = this.dialog.open(templateRef, {
         width: '250px',
    });
  }

  onTemplateCancel() {
    this.dialogRef.close(true);
  }

  deleteCategory() {
    this.templateService.deleteCategory(this.pickedCategory);
    this.dialogRef.close(true);
   }

  deleteTemplate(key) {
    this.templateService.deleteTemplate(key)
  }
}
